import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

/* Application Root */
import { BaseComponent } from './../components/base.component';

/* Application Base Modules */
import { AuthModule } from './auth/auth.module';

const appRoutes: Routes = [];

@NgModule({
    imports: [ 
        BrowserModule, 
        AuthModule,
        RouterModule.forRoot(appRoutes)
    ],
    declarations: [ BaseComponent ],
    bootstrap:    [ BaseComponent ]
})

export class BaseModule { }