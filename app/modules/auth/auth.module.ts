import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/* Authentication Components */
import { AuthComponent } from './../../components/auth/auth.component';
import { AuthLogInComponent } from './../../components/auth/auth-log-in.component';


const authComponents = [
    AuthComponent, 
    AuthLogInComponent
];

const authRoutes: Routes = [
    { 
        path: 'auth', 
        component: AuthComponent
    },
    { 
        path: 'auth', 
        component: AuthComponent,
        children:[
            {
             path : 'login',
             component: AuthLogInComponent
            }
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(authRoutes)
    ],
    declarations: authComponents,
    exports:      authComponents
})

export class AuthModule {}
