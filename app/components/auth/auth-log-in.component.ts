import { Component } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'auth-log-in',
  templateUrl: './app/templates/auth/auth-log-in.html'
})

export class AuthLogInComponent { 
    
    constructor(
      private location: Location
    ) {}
    
    goBack(): void {
      this.location.back();
    }

}
