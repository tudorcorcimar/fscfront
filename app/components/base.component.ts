import { Component } from '@angular/core';

@Component({
    selector: 'base-app',
    templateUrl: './app/templates/base.html'
})

export class BaseComponent { }