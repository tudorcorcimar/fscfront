import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { BaseModule } from '../modules/base.module';

const platform = platformBrowserDynamic();

platform.bootstrapModule(BaseModule);
